package ru.inshakov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.endpoint.SessionRecord;

@Getter
@Setter
@Service
public class SessionService {

    @Nullable
    private SessionRecord session;

}
