package ru.inshakov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.system.UnknownArgumentException;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.exception.system.UnknownCommandException;
import ru.inshakov.tm.service.ListenerService;
import ru.inshakov.tm.service.LoggerService;
import ru.inshakov.tm.util.SystemUtil;
import ru.inshakov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    protected FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ListenerService listenerService;

    @NotNull
    @Autowired
    private LoggerService loggerService;

    public void init() {
        initPID();
        initListeners();
    }

    public void run(@Nullable final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        init();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseCommand(@Nullable final String name) {
        if (name == null || name.isEmpty()) return;
        @Nullable final AbstractListener listener = listenerService.getListenerByName(name);
        if (listener == null) throw new UnknownCommandException(name);
        publisher.publishEvent(new ConsoleEvent(name));
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractListener listener = listenerService.getListenerByArg(args[0]);
        if (listener == null) throw new UnknownArgumentException(args[0]);
        publisher.publishEvent(new ConsoleEvent(listener.name()));
        return true;
    }

    @SneakyThrows
    private void initListeners(){
        if (listeners == null) return;
        Arrays.stream(listeners).filter(Objects::nonNull).forEach(t -> listenerService.add(t));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
