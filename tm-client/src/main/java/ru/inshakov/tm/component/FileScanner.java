package ru.inshakov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.service.ListenerService;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class FileScanner {
    @NotNull
    private static final String PATH = "./";

    private static final int INTERVAL = 10;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> listeners = new ArrayList<>();

    @Nullable
    @Autowired
    protected Bootstrap bootstrap;

    @NotNull
    @Autowired
    public ListenerService listenerService;

    public void init() {
        listeners.addAll(
                listenerService.getArguments().stream()
                        .map(AbstractListener::name)
                        .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && listeners.contains(o.getName()))
                .forEach(o -> {
                    bootstrap.parseCommand(o.getName());
                    o.delete();
                });
    }

}