package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.inshakov.tm.api.repository.IListenerRepository;
import ru.inshakov.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ListenerRepository implements IListenerRepository {

    @NotNull
    private final Map<String, AbstractListener> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractListener> listeners = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractListener> getListeners() {
        return listeners.values();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getListenerNames() {
        return listeners.values().stream()
                .filter(o -> o.name() != null && !o.name().isEmpty())
                .map(AbstractListener::name)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<String> getListenerArg() {
        return listeners.values().stream()
                .filter(o -> o.arg() != null && !o.arg().isEmpty())
                .map(AbstractListener::arg)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractListener getListenerByName(@NotNull String name) {
        return listeners.get(name);
    }

    @Nullable
    @Override
    public AbstractListener getListenerByArg(@NotNull String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull AbstractListener command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) listeners.put(name, command);
    }

}
