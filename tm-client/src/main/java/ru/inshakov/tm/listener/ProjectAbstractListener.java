package ru.inshakov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;

public abstract class ProjectAbstractListener extends AbstractListener {

    protected void show(@Nullable final ProjectRecord project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.valueOf(project.getStatus().value()).getDisplayName());
    }

    @NotNull
    protected ProjectRecord add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectRecord project = new ProjectRecord();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    protected String toString(ProjectRecord project) {
        return project.getId() + ": " + project.getName();
    }

}
