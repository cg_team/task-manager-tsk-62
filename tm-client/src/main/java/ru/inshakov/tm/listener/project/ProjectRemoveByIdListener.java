package ru.inshakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.listener.ProjectAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectRecord project = projectEndpoint.findProjectById(getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
        projectEndpoint.removeProjectById(getSession(), id);
    }

}
