package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.listener.TaskAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskBindTaskToProjectByIdListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    @EventListener(condition = "@taskBindTaskToProjectByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskRecord task = taskEndpoint.findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        taskEndpoint.bindTaskById(getSession(), taskId, projectId);
    }
}
