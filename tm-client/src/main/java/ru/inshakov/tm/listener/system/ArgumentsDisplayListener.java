package ru.inshakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.service.ListenerService;

@Component
public class ArgumentsDisplayListener extends AbstractListener {


    @NotNull
    @Autowired
    private ListenerService listenerService;

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        int index = 1;
        for (@NotNull final String arg : listenerService.getListListenerArg()) {
            System.out.println(index + ". " + arg);
            index++;
        }
    }

}
