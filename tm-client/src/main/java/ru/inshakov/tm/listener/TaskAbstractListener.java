package ru.inshakov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;


public abstract class TaskAbstractListener extends AbstractListener {

    protected void show(@Nullable final TaskRecord task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.valueOf(task.getStatus().value()).getDisplayName());
        System.out.println("ProjectRecord Id: " + task.getProjectId());
    }

    @NotNull
    protected TaskRecord add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskRecord task = new TaskRecord();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    protected String toString(TaskRecord task) {
        return task.getId() + ": " + task.getName();
    }

}
