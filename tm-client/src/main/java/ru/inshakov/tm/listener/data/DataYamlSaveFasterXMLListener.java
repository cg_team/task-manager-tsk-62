package ru.inshakov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.DataEndpoint;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AuthAbstractListener;

@Component
public class DataYamlSaveFasterXMLListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-save-yaml";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Save data to YAML by FasterXML.";
    }

    @EventListener(condition = "@dataYamlSaveFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        dataEndpoint.saveDataYaml(getSession());
    }

}