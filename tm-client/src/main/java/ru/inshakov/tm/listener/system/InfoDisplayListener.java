package ru.inshakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.util.NumberUtil;

@Component
public class InfoDisplayListener extends AbstractListener {
    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Display system info.";
    }

    @Override
    @EventListener(condition = "@infoDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.format(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }
}
