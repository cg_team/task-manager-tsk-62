package ru.inshakov.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AbstractListener;

@Component
public class AboutDisplayListener extends AbstractListener {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    @EventListener(condition = "@aboutDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }
}
