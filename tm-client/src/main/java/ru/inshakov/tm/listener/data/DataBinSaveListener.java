package ru.inshakov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.DataEndpoint;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AuthAbstractListener;

@Component
public class DataBinSaveListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-save-bin";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Save binary data";
    }

    @EventListener(condition = "@dataBinSaveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        dataEndpoint.saveDataBin(getSession());
    }

}