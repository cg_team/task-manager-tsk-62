package ru.inshakov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.DataEndpoint;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AuthAbstractListener;

@Component
public class DataXmlLoadJaxBListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-load-xml-j";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Load data from XML by JaxB.";
    }

    @EventListener(condition = "@dataXmlLoadJaxBListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        dataEndpoint.loadDataXmlJaxB(getSession());
    }

}