package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.TaskAbstractListener;

import java.util.List;

@Component
public class TaskShowListListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    @EventListener(condition = "@taskShowListListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable List<TaskRecord> tasks = taskEndpoint.findTaskAll(getSession());
        int index = 1;
        for (@NotNull TaskRecord task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
    }

}
