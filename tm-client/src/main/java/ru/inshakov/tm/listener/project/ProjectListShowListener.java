package ru.inshakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.ProjectAbstractListener;

import java.util.List;

@Component
public class ProjectListShowListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    @EventListener(condition = "@projectListShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter sort");
        @Nullable List<ProjectRecord> projects = projectEndpoint.findProjectAll(getSession());
        int index = 1;
        for (@NotNull final ProjectRecord project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
    }

}
