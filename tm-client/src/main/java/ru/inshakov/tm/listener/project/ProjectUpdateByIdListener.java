package ru.inshakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.listener.ProjectAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIdListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectRecord project = projectEndpoint.findProjectById(getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final ProjectRecord projectUpdated = projectEndpoint
                .updateProjectById(getSession(), id, name, description);
        if (projectUpdated == null) System.out.println("Incorrect value");
    }

}
