package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.TaskAbstractListener;

@Component
public class TaskClearListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    @EventListener(condition = "@taskClearListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        taskEndpoint.clearTask(getSession());
    }

}
