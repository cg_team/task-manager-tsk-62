package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.listener.TaskAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskRecord task = taskEndpoint.findTaskById(getSession(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final TaskRecord taskUpdated = taskEndpoint.updateTaskById(getSession(), id, name, description);
        if (taskUpdated == null) System.out.println("Incorrect value");
    }

}
