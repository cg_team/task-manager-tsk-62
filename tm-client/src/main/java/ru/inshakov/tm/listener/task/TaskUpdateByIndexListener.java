package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.listener.TaskAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIndexListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskRecord task = taskEndpoint.findTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final TaskRecord taskUpdated = taskEndpoint.updateTaskByIndex(getSession(), index, name, description);
        if (taskUpdated == null) System.out.println("Incorrect value");
    }

}
