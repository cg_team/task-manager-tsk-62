package ru.inshakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.listener.ProjectAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectStartByIndexListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by index.";
    }

    @Override
    @EventListener(condition = "@projectStartByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectRecord project = projectEndpoint.startProjectByIndex(getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
    }

}
