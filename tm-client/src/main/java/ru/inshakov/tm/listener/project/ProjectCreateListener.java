package ru.inshakov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.endpoint.ProjectRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.ProjectAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectRecord project = add(name, description);
        projectEndpoint.addProject(getSession(), project);
    }

}
