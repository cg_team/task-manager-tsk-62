package ru.inshakov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.AdminEndpoint;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class UserRemoveByLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by login";
    }

    @Override
    @EventListener(condition = "@userRemoveByLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        adminEndpoint.removeByLogin(getSession(), login);
    }

}
