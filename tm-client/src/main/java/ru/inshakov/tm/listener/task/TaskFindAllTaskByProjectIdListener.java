package ru.inshakov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.endpoint.TaskRecord;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.TaskAbstractListener;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskFindAllTaskByProjectIdListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-find-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in project by project id.";
    }

    @Override
    @EventListener(condition = "@taskFindAllTaskByProjectIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final List<TaskRecord> tasks = taskEndpoint.findTaskByProjectId(getSession(), id);
        System.out.println("TaskRecord list for project");
        for (@NotNull TaskRecord task : tasks) {
            System.out.println(task.toString());
        }
    }

}
