package ru.inshakov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inshakov.tm.dto.UserRecord;

public interface IUserRecordRepository extends JpaRepository<UserRecord, String> {

    UserRecord findByLogin(final String login);

    UserRecord findByEmail(final String email);

    void deleteByLogin(final String login);

}
