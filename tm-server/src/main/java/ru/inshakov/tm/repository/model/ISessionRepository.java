package ru.inshakov.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.inshakov.tm.model.SessionGraph;

import java.util.List;

public interface ISessionRepository extends JpaRepository<SessionGraph, String> {

    List<SessionGraph> findAllByUserId(@Nullable String userId);

    void deleteByUserId(@Nullable String userId);

}
