package ru.inshakov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.inshakov.tm.dto.ProjectRecord;

import java.util.List;

public interface IProjectRecordRepository extends JpaRepository<ProjectRecord, String> {

    ProjectRecord findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<ProjectRecord> findAllByUserId(final String userId);

    ProjectRecord findByUserIdAndName(final String userId, final String name);

    @Query("SELECT e FROM ProjectRecord e WHERE e.userId = :userId")
    ProjectRecord findByIndexAndUserId(final String userId, final int index);

    void deleteByUserIdAndName(final String userId, final String name);

}
