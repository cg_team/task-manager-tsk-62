package ru.inshakov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inshakov.tm.model.UserGraph;

public interface IUserRepository extends JpaRepository<UserGraph, String> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void deleteByLogin(final String login);

}
