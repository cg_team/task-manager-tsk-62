package ru.inshakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.endpoint.*;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

import static ru.inshakov.tm.util.SystemUtil.getPID;

@Getter
@Component
public class Bootstrap {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private static MessageExecutor messageExecutor;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private Backup backup;

    @Nullable
    @Autowired
    private AbstractEndpoint[] endpoints;


    public void start(String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        messageExecutor = context.getBean(MessageExecutor.class);
        //backup.init();
        initEndpoints();
    }

    public void initApplication() {
        initPID();
    }


    public void initEndpoints() {
        if (endpoints == null) return;
        Arrays.stream(endpoints).filter(Objects::nonNull).forEach(this::initEndpoints);
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    public void initEndpoints(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public static void sendMessage(@Nullable final Object record,
                                   @NotNull final String type) {
        messageExecutor.sendMessage(record, type);
    }

}
