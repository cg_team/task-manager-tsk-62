package ru.inshakov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.api.service.IMessageService;
import ru.inshakov.tm.dto.LoggerDTO;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class MessageExecutor {

    @NotNull
    private static final int THREAD_COUNT = 3;

    @NotNull
    @Autowired
    private IMessageService service;

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@Nullable final Object record,
                            @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LoggerDTO entity = service.prepareMessage(record, type);
            service.sendMessage(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }

}
