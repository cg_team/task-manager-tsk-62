package ru.inshakov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConst {

    @NotNull
    String CACHE_HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

}
