package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRecordService;
import ru.inshakov.tm.dto.TaskRecord;

import java.util.Collection;
import java.util.List;

public interface ITaskRecordService extends IRecordService<TaskRecord> {

    TaskRecord findByName(String userId, String name);

    TaskRecord findByIndex(String userId, Integer index);

    void removeByName(String userId, String name);

    TaskRecord updateById(String userId, final String id, final String name, final String description);

    TaskRecord updateByIndex(String userId, final Integer index, final String name, final String description);

    TaskRecord startById(String userId, String id);

    TaskRecord startByIndex(String userId, Integer index);

    TaskRecord startByName(String userId, String name);

    TaskRecord finishById(String userId, String id);

    TaskRecord finishByIndex(String userId, Integer index);

    TaskRecord finishByName(String userId, String name);

    TaskRecord add(String user, String name, String description);

    List<TaskRecord> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<TaskRecord> collection);

    TaskRecord add(String user, @Nullable TaskRecord entity);

    TaskRecord findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable TaskRecord entity);
}
