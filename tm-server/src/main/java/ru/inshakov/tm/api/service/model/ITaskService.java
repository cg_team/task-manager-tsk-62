package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.model.TaskGraph;
import ru.inshakov.tm.model.UserGraph;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<TaskGraph> {

    TaskGraph findByName(String userId, String name);

    void removeByName(String userId, String name);

    TaskGraph updateById(String userId, final String id, final String name, final String description);

    TaskGraph startById(String userId, String id);

    TaskGraph startByName(String userId, String name);

    TaskGraph finishById(String userId, String id);

    TaskGraph finishByName(String userId, String name);

    TaskGraph add(UserGraph user, String name, String description);

    List<TaskGraph> findAll(@NotNull String userId);

    void addAll(UserGraph user, @Nullable Collection<TaskGraph> collection);

    TaskGraph add(UserGraph user, @Nullable TaskGraph entity);

    TaskGraph findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable TaskGraph entity);
}
