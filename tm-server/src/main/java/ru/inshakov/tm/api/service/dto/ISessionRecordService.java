package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRecordService;
import ru.inshakov.tm.dto.SessionRecord;
import ru.inshakov.tm.dto.UserRecord;
import ru.inshakov.tm.enumerated.Role;

import java.util.List;

public interface ISessionRecordService extends IRecordService<SessionRecord> {

    SessionRecord open(@Nullable String login, @Nullable String password);

    UserRecord checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionRecord session, Role role);

    void validate(@Nullable SessionRecord session);

    SessionRecord sign(@Nullable SessionRecord session);

    void close(@Nullable SessionRecord session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionRecord> findAllByUserId(@Nullable String userId);
}
