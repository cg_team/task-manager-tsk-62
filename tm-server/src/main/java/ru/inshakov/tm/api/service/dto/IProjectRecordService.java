package ru.inshakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRecordService;
import ru.inshakov.tm.dto.ProjectRecord;

import java.util.Collection;
import java.util.List;

public interface IProjectRecordService extends IRecordService<ProjectRecord> {

    ProjectRecord findByName(final String userId, final String name);

    ProjectRecord findByIndex(final String userId, final Integer index);

    void removeByName(final String userId, final String name);

    ProjectRecord updateById(final String userId, final String id, final String name, final String description);

    ProjectRecord updateByIndex(final String userId, final Integer index, final String name, final String description);

    ProjectRecord startById(final String userId, final String id);

    ProjectRecord startByIndex(final String userId, final Integer index);

    ProjectRecord startByName(final String userId, final String name);

    ProjectRecord finishById(final String userId, final String id);

    ProjectRecord finishByIndex(final String userId, final Integer index);

    ProjectRecord finishByName(final String userId, final String name);

    ProjectRecord add(String userId, String name, String description);

    List<ProjectRecord> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<ProjectRecord> collection);

    ProjectRecord add(String userId, @Nullable ProjectRecord entity);

    ProjectRecord findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable ProjectRecord entity);
}
