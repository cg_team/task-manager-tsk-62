package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.model.ProjectGraph;
import ru.inshakov.tm.model.UserGraph;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<ProjectGraph> {

    ProjectGraph findByName(final String userId, final String name);

    void removeByName(final String userId, final String name);

    ProjectGraph updateById(final String userId, final String id, final String name, final String description);

    ProjectGraph startById(final String userId, final String id);

    ProjectGraph startByName(final String userId, final String name);

    ProjectGraph finishById(final String userId, final String id);

    ProjectGraph finishByName(final String userId, final String name);

    ProjectGraph add(UserGraph user, String name, String description);

    List<ProjectGraph> findAll(@NotNull String userId);

    void addAll(UserGraph user, @Nullable Collection<ProjectGraph> collection);

    ProjectGraph add(UserGraph user, @Nullable ProjectGraph entity);

    ProjectGraph findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable ProjectGraph entity);
}
