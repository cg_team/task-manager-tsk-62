package ru.inshakov.tm.api.service.dto;

import ru.inshakov.tm.api.IRecordService;
import ru.inshakov.tm.dto.UserRecord;

public interface IUserRecordService extends IRecordService<UserRecord> {

    UserRecord findByLogin(final String login);

    UserRecord findByEmail(final String email);

    void removeByLogin(final String login);

    UserRecord add(final String login, final String password);

    UserRecord add(final String login, final String password, final String email);

    UserRecord setPassword(final String id, final String password);

    boolean isLoginExist(final String login);

    boolean isEmailExist(final String email);

    UserRecord updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    );

    UserRecord lockByLogin(final String login);

    UserRecord unlockByLogin(final String login);

}
