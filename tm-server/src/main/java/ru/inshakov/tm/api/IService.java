package ru.inshakov.tm.api;


import ru.inshakov.tm.model.AbstractGraph;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractGraph> {

    List<E> findAll();

    E add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void remove(final E entity);

    void removeById(final String id);

}