package ru.inshakov.tm.api.service;

public interface ILogService {

    void info(String message);

    void debug(String message);

    void command(String message);

    void error(Exception e);

}
