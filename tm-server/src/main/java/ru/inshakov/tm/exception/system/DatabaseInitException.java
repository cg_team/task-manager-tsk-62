package ru.inshakov.tm.exception.system;

public final class DatabaseInitException extends RuntimeException {

    public DatabaseInitException() {
        super("Error! Connection failed...");
    }

}