package ru.inshakov.tm.exception.empty;

public final class EmptyIndexException extends RuntimeException {

    public EmptyIndexException() {
        super("Error! Index is empty...");
    }

}
