package ru.inshakov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.inshakov.tm.api.service.IDataService;
import ru.inshakov.tm.dto.SessionRecord;
import ru.inshakov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
public final class DataEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IDataService dataService;

    @WebMethod
    @SneakyThrows
    public void loadDataBase64(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataBase64();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBase64(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataBase64();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBin(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataBin();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBin(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataBin();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJson(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataJson();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJson(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataJson();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXml(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataXml();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXml(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataXml();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataYaml(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataYaml();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataYaml(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataYaml();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonJaxB(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataJsonJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonJaxB(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataJsonJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlJaxB(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.loadDataXmlJaxB();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlJaxB(@WebParam(name = "session") final SessionRecord session) {
        sessionRecordService.validate(session, Role.ADMIN);
        dataService.saveDataXmlJaxB();
    }
}
