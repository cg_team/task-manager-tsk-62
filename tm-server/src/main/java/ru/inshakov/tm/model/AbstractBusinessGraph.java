package ru.inshakov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Setter
@Getter
@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractBusinessGraph extends AbstractGraph {

    @Nullable
    @ManyToOne
    @JsonIgnore
    protected UserGraph user;

}