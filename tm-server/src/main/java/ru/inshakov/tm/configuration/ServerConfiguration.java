package ru.inshakov.tm.configuration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.exception.system.DatabaseInitException;

import javax.sql.DataSource;
import java.util.Properties;

import static ru.inshakov.tm.constant.PropertyConst.CACHE_HAZELCAST_USE_LITE_MEMBER;

@Configuration
@ComponentScan("ru.inshakov.tm")
@EnableJpaRepositories("ru.inshakov.tm.repository")
@EnableTransactionManagement(proxyTargetClass = true)
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Bean
    public DataSource dataSource(
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new DatabaseInitException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new DatabaseInitException();
        @Nullable final String password = propertyService.getJdbcPassword();
        if (password == null) throw new DatabaseInitException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new DatabaseInitException();

        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.inshakov.tm.model", "ru.inshakov.tm.dto");

        @NotNull final Properties properties = new Properties();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        if (dialect == null) throw new DatabaseInitException();
        @Nullable final String auto = propertyService.getHibernateBM2DDLAuto();
        if (auto == null) throw new DatabaseInitException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new DatabaseInitException();
        @Nullable final String secondLevelCash = propertyService.getSecondLevelCash();
        if (secondLevelCash == null) throw new DatabaseInitException();
        @Nullable final String queryCache = propertyService.getQueryCache();
        if (queryCache == null) throw new DatabaseInitException();
        @Nullable final String minimalPuts = propertyService.getMinimalPuts();
        if (minimalPuts == null) throw new DatabaseInitException();
        @Nullable final String regionPrefix = propertyService.getRegionPrefix();
        if (regionPrefix == null) throw new DatabaseInitException();
        @Nullable final String cacheProvider = propertyService.getCacheProvider();
        if (cacheProvider == null) throw new DatabaseInitException();
        @Nullable final String factoryClass = propertyService.getFactoryClass();
        if (factoryClass == null) throw new DatabaseInitException();
        @Nullable final String liteMember = propertyService.getLiteMember();
        if (liteMember == null) throw new DatabaseInitException();

        properties.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        properties.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        properties.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);
        if ("true".equals(secondLevelCash)) {
            properties.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, secondLevelCash);
            properties.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, queryCache);
            properties.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, minimalPuts);
            properties.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, regionPrefix);
            properties.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, cacheProvider);
            properties.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, factoryClass);
            properties.put(CACHE_HAZELCAST_USE_LITE_MEMBER, liteMember);
        }

        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
