package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.service.model.IProjectService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.ProjectGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.IProjectRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<ProjectGraph> collection) {
        if (collection == null) return;
        for (ProjectGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public ProjectGraph add(@Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<ProjectGraph> projects = repository.findAll();
        for (ProjectGraph t :
                projects) {
            repository.delete(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final ProjectGraph project = repository.findById(optionalId.orElseThrow(EmptyIdException::new))
                .orElse(null);
        if (project == null) return;
        repository.delete(project);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @Nullable final ProjectGraph project = repository.findById(entity.getId()).orElse(null);
        if (project == null) return;
        repository.delete(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectGraph project = repository.findByUserIdAndName(userId, name);
        if (project == null) return;
        repository.delete(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.save(project);
        return project;
    }

    @Nullable
    public ProjectGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = new ProjectGraph(name, description);
        project.setUser(user);
        return add(project);
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull final String userId) {

        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<ProjectGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (ProjectGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph add(@NotNull final UserGraph user, @Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final ProjectGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final List<ProjectGraph> projects = repository.findAllByUserId(userId);
        for (ProjectGraph t :
                projects) {
            repository.delete(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final ProjectGraph project = repository.findByUserIdAndId(
                userId,
                optionalId.orElseThrow(EmptyIdException::new)
        );
        if (project == null) return;
        repository.delete(project);
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @Nullable final ProjectGraph project = repository.findByUserIdAndId(userId, entity.getId());
        if (project == null) return;
        repository.delete(project);
    }

}
