package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.model.ISessionService;
import ru.inshakov.tm.api.service.model.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.system.AccessDeniedException;
import ru.inshakov.tm.model.SessionGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.ISessionRepository;
import ru.inshakov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SessionGraphService extends AbstractGraphService<SessionGraph> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionGraph> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<SessionGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (SessionGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph add(@Nullable final SessionGraph entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<SessionGraph> sessions = repository.findAll();
        for (SessionGraph t :
                sessions) {
            repository.delete(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final SessionGraph session = repository
                .findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
        if (session == null) return;
        repository.delete(session);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionGraph entity) {
        if (entity == null) return;
        @Nullable final SessionGraph session = repository.findById(entity.getId()).orElse(null);
        if (session == null) return;
        repository.delete(session);
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionGraph open(@Nullable final String login, @Nullable final String password) {
        @Nullable final UserGraph user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final SessionGraph session = new SessionGraph();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionGraph resultSession = sign(session);
        add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public UserGraph checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final UserGraph user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionGraph session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUser().getId();
        @Nullable final UserGraph user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId() == null || session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (repository.findById(session.getId()) == null) throw new AccessDeniedException();

    }

    @Override
    @SneakyThrows
    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final SessionGraph session) {
        @Nullable final SessionGraph sessionReference = repository.findById(session.getId()).orElse(null);
        if (sessionReference == null) return;
        repository.delete(sessionReference);
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final List<SessionGraph> sessions = repository.findAllByUserId(userId);
        for (SessionGraph t :
                sessions) {
            repository.delete(t);
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<SessionGraph> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        return repository.findAllByUserId(userId);
    }
}
