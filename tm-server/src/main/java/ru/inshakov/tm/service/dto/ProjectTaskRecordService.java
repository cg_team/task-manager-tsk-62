package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.service.dto.IProjectTaskRecordService;
import ru.inshakov.tm.dto.TaskRecord;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.repository.dto.IProjectRecordRepository;
import ru.inshakov.tm.repository.dto.ITaskRecordRepository;
import ru.inshakov.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectTaskRecordService extends AbstractService implements IProjectTaskRecordService {

    @NotNull
    @Autowired
    private ITaskRecordRepository repository;

    @NotNull
    @Autowired
    private IProjectRecordRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskRecord> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return repository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        repository.unbindTaskById(userId, taskId);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndexAndUserId(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByUserIdAndName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        repository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

}