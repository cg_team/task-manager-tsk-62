package ru.inshakov.tm.service.dto;

import ru.inshakov.tm.api.IRecordService;
import ru.inshakov.tm.dto.AbstractRecord;
import ru.inshakov.tm.service.AbstractService;

public abstract class AbstractRecordService<E extends AbstractRecord> extends AbstractService implements IRecordService<E> {

}
