package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.inshakov.tm.api.service.dto.ITaskRecordService;
import ru.inshakov.tm.dto.TaskRecord;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.repository.dto.ITaskRecordRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskRecordService extends AbstractRecordService<TaskRecord> implements ITaskRecordService {

    @NotNull
    @Autowired
    private ITaskRecordRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskRecord> collection) {
        if (collection == null) return;
        for (TaskRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(@Nullable final TaskRecord entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskRecord entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndexAndUserId(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.deleteByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByUserIdAndId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByIndexAndUserId(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRecord finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskRecord task = Optional.ofNullable(repository.findByUserIdAndName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.save(task);
        return task;
    }

    @SneakyThrows
    @Nullable
    public TaskRecord add(String user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskRecord task = new TaskRecord(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskRecord> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<TaskRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        addAll(collection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord add(final String user, @Nullable final TaskRecord entity) {
        if (entity == null) return null;
        entity.setUserId(user);
        @Nullable final TaskRecord entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskRecord findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskRecord entity) {
        if (entity == null) return;
        repository.deleteByUserIdAndId(userId, entity.getId());
    }

}
